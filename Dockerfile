from python:slim
RUN pip install ipapi
RUN pip install flask
COPY flaskipfinder.py .
COPY templates .
EXPOSE 80
ENTRYPOINT ["python", "flaskipfinder.py"]
