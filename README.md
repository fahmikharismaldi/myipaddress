# myipaddress

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
Ensure your system have installed docker and git.
Clone this repository https://gitlab.com/fahmikharismaldi/myipaddress.git

### Build Image

Follow these instructions to build docker image.

```
cd myipaddress
docker build -t myipaddress:latest
```

### Run Image

Follow these instructions to run your docker image.

```
docker-compose up -d
```

